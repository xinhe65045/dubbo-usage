package org.xinhe.dubbo.usage.simple.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>Title: Order</p>
 * <p>Date: 2018/4/21 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class Order {
    // ID
    private Long id;
    // 所属公司
    private Integer adClientId;
    // 所属组织
    private Integer adOrgId;
    // 单据编号
    private String billNo;
    // 超量加单
    private Integer isOverAdd;
    // 单据日期
    private Date billDate;
    // 原单号ID
    private Long reqBillId;
    // 原单号编码
    private String reqBillNo;
    // 原单号ID
    private Long fromBillId;
    // 原单号编码
    private String fromBillNo;
    // 订单类型
    private String billType;
    // 供应商ID
    private Long cpCSupplierId;
    // 供应商编码
    private String cpCSupplierEcode;
    // 店仓ID
    private Long cpCStoreId;
    // 店仓编码
    private String cpCStoreEcode;
    // 有效日期
    private Date dateIn;
    // 采购原因
    private String purReason;
    // 计划铺店数
    private Integer planStores;
    // 预计到仓日期
    private Date estimatedDeliveryDate;
    // 上市日期
    private Date releaseDate;
    // 明细款
    private String psCProEcodeList;
    // 采购数量合计
    private BigDecimal sumQtyBill;
    // 已入库数量合计
    private BigDecimal sumQtyIn;
    // 执行数量
    private BigDecimal sumQtyExecute;
    // 剩余数量合计
    private BigDecimal sumQtyRem;
    // 采购金额合计
    private BigDecimal sumAmtActual;
    // 已入库金额合计
    private BigDecimal sumAmtIn;
    // 剩余金额合计
    private BigDecimal sumAmtRem;
    // 备注
    private String remark;
    // 创建人ID
    private Long ownerId;
    // 创建人姓名
    private String ownerEname;
    // 创建人用户名
    private String ownerName;
    // 创建时间
    private Date creationDate;
    // 修改人ID
    private Long modifierId;
    // 修改人姓名
    private String modifierEname;
    // 修改人用户名
    private String modifierName;
    // 修改时间
    private Date modifiedTime;
    // 提交标志
    private Integer submitStatus;
    // 提交人ID
    private Long submiterId;
    // 提交人用户名
    private String submiterName;
    // 提交人姓名
    private String submiterEname;
    // 提交时间
    private Date submitTime;
    // 作废标志
    private Integer delStatus;
    // 作废人ID
    private Long delerId;
    // 作废人用户名
    private String delerName;
    // 作废人姓名
    private String delerEname;
    // 作废时间
    private Date delTime;
    // 终止标志
    private Integer terminateStatus;
    // 终止人ID
    private Long terminatorId;
    // 终止人用户名
    private String terminatorName;
    // 终止人姓名
    private String terminatorEname;
    // 终止时间
    private Date terminateTime;
    // 版本号，用于并发控制
    private Integer version;




}
