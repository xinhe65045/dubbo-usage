package org.xinhe.dubbo.usage.simple.service;

/**
 * <p>Title: SimpleService</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/3
 */
public interface SimpleService {

    /**
     * 环回接口，输入一个字符串，返回原值
     *
     * @param arg 入参字符串
     * @return String
     */
    String loopbackStr(String arg);

    String loopback();

    Integer loopbackInteger(Integer arg);
}
