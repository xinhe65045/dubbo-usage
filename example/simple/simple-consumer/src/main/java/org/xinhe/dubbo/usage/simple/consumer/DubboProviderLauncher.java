package org.xinhe.dubbo.usage.simple.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>Title: DubboProviderLauncher</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/3
 */
@SpringBootApplication
public class DubboProviderLauncher {
    public static void main(String[] args) {
        SpringApplication.run(DubboProviderLauncher.class, args);
    }

}
