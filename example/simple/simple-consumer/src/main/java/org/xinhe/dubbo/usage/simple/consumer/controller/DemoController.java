package org.xinhe.dubbo.usage.simple.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xinhe.dubbo.usage.simple.service.SimpleService;

/**
 * <p>Title: DemoController</p>
 * <p>Date: 2018/3/16 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @Reference
    private SimpleService simpleService;

    @RequestMapping("/loopback/integer")
    public Integer loopbackInteger(Integer num) {
        System.out.println(num);
        return simpleService.loopbackInteger(num);
    }

}
