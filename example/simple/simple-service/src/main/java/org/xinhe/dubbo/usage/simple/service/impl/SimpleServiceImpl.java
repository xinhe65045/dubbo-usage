package org.xinhe.dubbo.usage.simple.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.xinhe.dubbo.usage.simple.service.SimpleService;

/**
 * <p>Title: SimpleServiceImpl</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/3
 */
@Service(interfaceClass = SimpleService.class)
public class SimpleServiceImpl implements SimpleService {


    /**
     * @param arg 入参字符串
     * @return
     * @see SimpleService#loopbackStr(String)
     */
    public String loopbackStr(String arg) {
        return arg;
    }

    public String loopback() {
        return "foo";
    }

    public Integer loopbackInteger(Integer arg) {
        return arg;
    }
}
