package org.xinhe.dubbo.usage.simple.gateway.configuration;

import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.ZuulProxyConfiguration;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.route.SimpleHostRoutingFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xinhe.dubbo.usage.simple.gateway.routing.DubboRoutingFilter;

/**
 * <p>Title: CustomZuulProxyConfiguration</p>
 * <p>Date: 2018/7/25 </p>
 * <p>Description: TODO 这里是简单的替换，还需要考虑多协议自动适配的场景
 * @see com.netflix.zuul.ZuulFilter#filterDisabled </p>
 *
 * @author sunxinhe
 */
@Configuration
@EnableZuulProxy
public class CustomZuulProxyConfiguration extends ZuulProxyConfiguration {
    @Bean
    @Override
    public SimpleHostRoutingFilter simpleHostRoutingFilter(ProxyRequestHelper helper,
                                                           ZuulProperties zuulProperties) {
        return new DubboRoutingFilter(helper, zuulProperties);
    }



}
