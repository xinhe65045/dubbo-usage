package org.xinhe.dubbo.usage.simple.gateway.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.netflix.zuul.filters.SimpleRouteLocator;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xinhe.dubbo.usage.simple.gateway.routing.MyRouteLocator;

@Configuration
public class GatewayAutoConfiguration {

    @Autowired
    ZuulProperties zuulProperties;
    @Autowired
    ServerProperties server;

    @Bean
    public SimpleRouteLocator routeLocator() {
        MyRouteLocator routeLocator = new MyRouteLocator(this.server.getServletPrefix(), this.zuulProperties);
        return routeLocator;
    }

}
