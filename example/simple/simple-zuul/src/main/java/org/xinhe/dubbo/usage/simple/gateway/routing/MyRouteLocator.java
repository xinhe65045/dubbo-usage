package org.xinhe.dubbo.usage.simple.gateway.routing;

import org.springframework.cloud.netflix.zuul.filters.RefreshableRouteLocator;
import org.springframework.cloud.netflix.zuul.filters.SimpleRouteLocator;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>Title: MyRouteLocator</p>
 * <p>Date: 2018/7/25 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class MyRouteLocator extends SimpleRouteLocator implements RefreshableRouteLocator {

    public MyRouteLocator(String servletPath, ZuulProperties properties) {
        super(servletPath, properties);
    }

    @Override
    public void refresh() {

    }

    @Override
    protected Map<String, ZuulProperties.ZuulRoute> locateRoutes() {
        LinkedHashMap<String, ZuulProperties.ZuulRoute> values = new LinkedHashMap<>();
        ZuulProperties.ZuulRoute route = new ZuulProperties.ZuulRoute();
        route.setPath("/foo");
        values.put("/foo", route);
        return values;
    }
}
