package org.xinhe.dubbo.usage.core.exception;

/**
 * <p>Title: DubboSimpleLogicException</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/4
 */
public class DubboSimpleLogicException extends LogicException {
    public DubboSimpleLogicException() {
        super();
    }

    public DubboSimpleLogicException(String msg) {
        super(msg);
    }

    public DubboSimpleLogicException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public DubboSimpleLogicException(String msg, String errorCode) {
        super(msg, errorCode);
    }

    public DubboSimpleLogicException(String msg, Throwable throwable, String errorCode) {
        super(msg, throwable, errorCode);
    }

    @Override
    public String getErrorCodePrifix() {
        return LogicException.ERROR_CODE_PREFIX_DUBBO_SIMPLE_DEFAULT;
    }
}
