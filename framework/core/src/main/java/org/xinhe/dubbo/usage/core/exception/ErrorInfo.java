package org.xinhe.dubbo.usage.core.exception;

/**
 * <p>Title: ErrorInfo</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/4
 */
public interface ErrorInfo {

    LogicException generateException();

    String getMsg();

    String getErrorCode();
}
