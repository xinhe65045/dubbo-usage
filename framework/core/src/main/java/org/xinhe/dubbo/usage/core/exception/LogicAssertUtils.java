package org.xinhe.dubbo.usage.core.exception;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

/**
 * <p>Title: LogicAssertUtils</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/4
 */
public class LogicAssertUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogicAssertUtils.class);

    /**
     * 抛出异常并打印日志及异常堆栈
     *
     * @param exceptionEnum 异常枚举
     * @param detail        异常明细信息
     */
    private static void throwAndLog(ErrorInfo exceptionEnum, String... detail) {
        LogicException e = exceptionEnum.generateException();
        LOGGER.error("业务异常：code={}, msg={}, detail={} ", e.getErrorCode(), e.getMsg(), detail, e);
        throw e;
    }

    /**
     * 判断传入的参数：text 是否为NULL或空白字符串
     * 如果是，抛出异常
     * 其中判断使用了spring的StringUtil
     *
     * @param text          待判断的字符串
     * @param exceptionEnum 指定的异常Enum
     * @param detail        异常详细描述
     * @see StringUtils#isBlank(String)
     */
    public static void hasText(String text, ErrorInfo exceptionEnum, String... detail) {
        if (StringUtils.isBlank(text)) {
            throwAndLog(exceptionEnum, detail);
        }
    }

    /**
     * 判断传入的参数：collection 是否empty
     * 如果是，抛出异常
     *
     * @param collection    待判断的集合
     * @param exceptionEnum 指定的异常Enum
     * @param detail        异常详细描述
     */
    public static void notEmpty(Collection collection, ErrorInfo exceptionEnum, String... detail) {
        if (CollectionUtils.isEmpty(collection)) {
            throwAndLog(exceptionEnum, detail);
        }
    }

    /**
     * 判断传入的参数：map 是否empty
     * 如果是，抛出异常
     *
     * @param map           待判断的map
     * @param exceptionEnum 指定的异常Enum
     * @param detail        异常详细描述
     */
    public static void notEmpty(Map map, ErrorInfo exceptionEnum, String... detail) {
        if (MapUtils.isEmpty(map)) {
            throwAndLog(exceptionEnum, detail);
        }
    }

    /**
     * 判断传入的参数：obj 是否Null
     * 如果是，抛出异常
     *
     * @param obj           待判断的obj
     * @param exceptionEnum 指定的异常Enum
     * @param detail        异常详细描述
     */
    public static void notNull(Object obj, ErrorInfo exceptionEnum, String... detail) {
        if (obj == null) {
            throwAndLog(exceptionEnum, detail);
        }
    }

    /**
     * 判断传入的参数：flag 是否true
     * 如果是，抛出异常
     *
     * @param flag           待判断的obj
     * @param exceptionEnum 指定的异常Enum
     * @param detail        异常详细描述
     */
    public static void isTrue(boolean flag, ErrorInfo exceptionEnum, String... detail) {
        if (flag == false) {
            throwAndLog(exceptionEnum, detail);
        }
    }
}
