package org.xinhe.dubbo.usage.core.exception;

/**
 * <p>Title: LogicException</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/4
 */
public class LogicException extends RuntimeException {

    public static final String ERROR_CODE_PREFIX_DEFAULT = "ERR_DEFAULT_";
    public static final String ERROR_CODE_PREFIX_DUBBO_SIMPLE_DEFAULT = "ERR_DUBBO_SIMPLE_";

    private String msg; //错误消息
    private String errorCode;   //异常码
    private Throwable throwable;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErrorCode() {
        return getErrorCodePrifix() + errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public String getErrorCodePrifix() {
        return ERROR_CODE_PREFIX_DEFAULT;
    }


    public LogicException() {
        super();
    }

    public LogicException(String msg) {
        super(msg);
        this.msg = msg;

    }

    public LogicException(String msg, Throwable throwable) {
        super(msg, throwable);
        this.msg = msg;
        this.throwable = throwable;
    }

    public LogicException(String msg, String errorCode) {
        this(msg, null, errorCode);
    }

    public LogicException(String msg, Throwable throwable, String errorCode) {
        super(msg);
        this.msg = msg;
        this.throwable = throwable;
        this.errorCode = errorCode;
    }
}
