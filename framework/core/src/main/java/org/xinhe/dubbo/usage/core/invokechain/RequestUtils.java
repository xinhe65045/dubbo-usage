package org.xinhe.dubbo.usage.core.invokechain;

/**
 * <p>Title: RequestUtils</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/4
 */
public class RequestUtils {
    private static ThreadLocal<String> requestId = new ThreadLocal<>();

    public static String getRequestId() {
        return requestId.get();
    }

    public static void setRequestId(String requestId) {
        RequestUtils.requestId.set(requestId);
    }
}
