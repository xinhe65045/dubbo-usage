package org.xinhe.dubbo.usage.core.invokechain.log;

/**
 * <p>Title: PatternLayout</p>
 * <p>Description: 统一输出requestId</p>
 *
 * @author xinhe.sun
 * @date 16/8/12
 */
public class PatternLayout extends ch.qos.logback.classic.PatternLayout {

    static {
        defaultConverterMap.put("rid", RequestIdConvert.class.getName());
    }
}