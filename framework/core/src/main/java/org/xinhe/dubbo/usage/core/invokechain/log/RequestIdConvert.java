package org.xinhe.dubbo.usage.core.invokechain.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.xinhe.dubbo.usage.core.invokechain.RequestUtils;

/**
 * <p>Title: RequestIdConvert</p>
 * <p>Description: 用于在RPC环境下转换requestId至日志输出 </p>
 * logback.xml 配置如下
 * <pre>
 * <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
 *      <layout class="cn.evun.sweet.ext.log.PatternLayout">
 *          <Pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %rid [%thread] %-5level %logger{10} -%msg%n</Pattern>
 *      </layout>
 * </appender>
 * </pre>
 *
 * @author xinhe.sun
 * @date 16/8/12«
 */
public class RequestIdConvert extends ClassicConverter {

    private static final String CONVERT = "[@rId: %s]";

    @Override
    public String convert(ILoggingEvent event) {
        return String.format(CONVERT, RequestUtils.getRequestId());
    }
}