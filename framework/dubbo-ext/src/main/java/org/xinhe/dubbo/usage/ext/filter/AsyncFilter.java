package org.xinhe.dubbo.usage.ext.filter;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;

/**
 * <p>Title: AsyncFilter</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/10/24
 */
@Activate(group = {Constants.PROVIDER})
public class AsyncFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        RpcContext.getContext().getAttachments().remove(Constants.ASYNC_KEY);
        return invoker.invoke(invocation);
    }
}
