package org.xinhe.dubbo.usage.ext.filter;

import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;

/**
 * <p>Title: ForwardRequestIdFilter</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/10/24
 */
@Activate(
        group = {"consumer"}
)
public class ForwardRequestIdFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        // before filter ...
        String requestId = RpcContext.getContext().getAttachment("requestId");
        RpcContext.getContext().setAttachment("requestId", requestId);

        // after filter ...
        return invoker.invoke(invocation);
    }
}
