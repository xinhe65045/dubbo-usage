package org.xinhe.dubbo.usage.ext.filter;

import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import org.xinhe.dubbo.usage.core.invokechain.RequestUtils;

/**
 * <p>Title: RequestIdFilter</p>
 * <p>Description: </p>
 *
 * @author xinhe.sun
 * @date 16/11/4
 */
@Activate(
        group = {"consumer"}
)
public class RequestIdFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        // before filter ...
        String requestId = RequestUtils.getRequestId();
        RpcContext.getContext().setAttachment("requestId", requestId);

        // after filter ...
        return invoker.invoke(invocation);
    }
}
