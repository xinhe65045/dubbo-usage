package org.xinhe.dubbo.usage.ext.log;

import ch.qos.logback.classic.PatternLayout;

/**
 * <p>Title: RpcPatternLayout</p>
 * <p>Description: RPC环境下统一输出requestId</p>
 *
 * @author xinhe.sun
 * @date 16/8/12
 */
public class RpcPatternLayout extends PatternLayout {

    static {
        defaultConverterMap.put("rid", RequestIdConvert.class.getName());
    }
}