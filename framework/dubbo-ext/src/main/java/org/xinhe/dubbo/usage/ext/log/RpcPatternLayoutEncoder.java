package org.xinhe.dubbo.usage.ext.log;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.pattern.PatternLayoutEncoderBase;

/**
 * <p>Title: RpcPatternLayoutEncoder</p>
 * <p>Description: 用于logback的layoutEncoder，使用RpcPatternLayout提供的日志扩展 </p>
 * <p/>
 * <pre>
 * <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
 *      <encoder charset="UTF-8" class="com.geely.dcs.baseinfo.log.RpcPatternLayoutEncoder">
 *          <pattern>%-5level %d{yyyy-MM-dd HH:mm:ss} [%rid] [%thread] %logger{10} -%msg%n</pattern>
 *      </encoder>
 * </appender>
 * </pre> * @author xinhe.sun
 *
 * @date 16/8/12
 */
public class RpcPatternLayoutEncoder extends PatternLayoutEncoderBase<ILoggingEvent> {

    @Override
    public void start() {
        RpcPatternLayout patternLayout = new RpcPatternLayout();
        patternLayout.setContext(context);
        patternLayout.setPattern(getPattern());
        patternLayout.setOutputPatternAsHeader(outputPatternAsHeader);
        patternLayout.start();
        this.layout = patternLayout;
        super.start();
    }
}